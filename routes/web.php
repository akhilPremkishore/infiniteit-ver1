<?php

use Illuminate\Support\Facades\Route;

use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome') -> with('users', User::orderBy('id', 'desc')->get());
});

Route::post('/save', 'UserController@save')->name('save');
Route::post('/status-change', 'UserController@statusChange')->name('status-change');
Route::post('/user-details', 'UserController@userDetails')->name('user-details');
Route::post('/update-user', 'UserController@userUpdate')->name('update-user');
Route::get('export', 'UserController@export')->name('export');

