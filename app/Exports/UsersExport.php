<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::select('first_name', 'last_name', 'mobile', 'email', 'status')
                    ->join('status', 'status.user_id', '=', 'users.id')
                    ->get();
    }

    public function headings(): array
    {
        return ["First Name", "Last Name", "Mobile", "Email", "Status"];
    }
}
