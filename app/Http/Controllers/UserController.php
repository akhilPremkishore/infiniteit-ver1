<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Status;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function save(Request $request){

        $validator  = Validator::make($request->all(),[
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'required|unique:users,email',
                'mobile'     => 'required|unique:users,mobile'
            ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => 'error']);
        }else{
            try {
                $image = $newImage = '';
                $user              = new User();
                $status            = new Status();
                $user->first_name  = $request->first_name;
                $user->last_name   = $request->last_name;
                $user->mobile      = $request->mobile;
                $user->email       = $request->email;
                if($request->file('profile')){
                    $validator  = Validator::make($request->all(),[
                                        'profile'  => 'image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'profile.mimes'    => 'Main Picture , Allowed image types are pngmjpg,jpeg',
                                        'profile.max'      => 'Main picture , Maximum file size allowed is 2 MB'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image             = $request->file('profile');
                        $newImage          = time().'.'.$image->getClientOriginalExtension();
                    }
                }
                $user->image   = $newImage;
                DB::transaction(function() use ($user, $status) {
                    $user->save();
                    $status->user_id = $user->id;
                    $status->save();
                });
                if(!empty($image)){
                    $image->move(public_path('images/'),$newImage);
                }
                return response()->json(['message' => 'User added successfully', 'status' => 'success']);
            }catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage(), 'status' => 'error']);
            }

        }
    }

    public function statusChange(Request $request){

        try{
            Status::where('user_id', $request->user_id)->update(['status' => $request->status]);
            return response()->json(['message' => 'User status changed successfully', 'status' => 'success']);
        }catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => 'error']);
        }
    }

    public function userDetails(Request $request){

        $userDetails = User::where('id',$request->user_id)->first();
        return response()->json(['message' => $userDetails, 'status' => 'success']);
    }

    public function userUpdate(Request $request){

        $user  = User::where('id', $request->id)->first();
        $validator  = Validator::make($request->all(),[
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required|unique:users,email,'.$user->id,
            'mobile'     => 'required|unique:users,mobile,'.$user->id
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => 'error']);
        }else{
            try {
                $image = '';
                $user->first_name  = $request->first_name;
                $user->last_name   = $request->last_name;
                $user->mobile      = $request->mobile;
                $user->email       = $request->email;
                if($request->file('profile')){
                    $validator  = Validator::make($request->all(),[
                                        'profile'  => 'image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'profile.mimes'    => 'Main Picture , Allowed image types are pngmjpg,jpeg',
                                        'profile.max'      => 'Main picture , Maximum file size allowed is 2 MB'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image             = $request->file('profile');
                        $newImage          = time().'.'.$image->getClientOriginalExtension();

                    }
                    $user->image   = $newImage;
                }
                DB::transaction(function() use ($user) {
                    $user->save();
                });
                if(!empty($image)){
                    File::delete(public_path().'/images/'.$user->image);
                    $image->move(public_path('images/'),$newImage);
                }
                return response()->json(['message' => 'User updated successfully', 'status' => 'success']);
            }catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage(), 'status' => 'error']);
            }
        }
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
